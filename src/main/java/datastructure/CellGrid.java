package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] cellState;

    public CellGrid(int rows, int columns, CellState initialState) {
		if(rows <= 0) {
            throw new IllegalArgumentException("Grid need at least 1 row, you supplied " + rows);
        }
        this.rows = rows;
        if(columns <= 0) {
            throw new IllegalArgumentException("Grid need at least 1 column, you supplied " + columns);
        }
        this.cols = columns;
        cellState = new CellState[this.rows][this.cols];
        for(int row = 0; row < rows; row++){
            for(int col = 0; col < cols; col++){
                cellState[row][col] = initialState;
            }
        }

	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        outOfBounds(row, column);
        cellState[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        outOfBounds(row, column);
        return cellState[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid copyCellGrid = new CellGrid(rows, cols, null);
        for (int row = 0; row < rows; row++){
            for(int col = 0; col < cols; col++){
                copyCellGrid.set(row, col, this.get(row, col));
            }
        }
        return copyCellGrid;
    }

    private void outOfBounds(int row, int column) {
        if(row >= rows || row < 0) {
            throw new IndexOutOfBoundsException("Grid has " + rows + ", you supplied " + row);
        }
        if(column >= cols || column < 0) {
            throw new IndexOutOfBoundsException("Grid has " + cols + ", you supplied " + column);
        }
    }
}
