package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton{

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public void step() {
        // finds the next step, updates copy
        IGrid nextGeneration = currentGeneration.copy();
        for(int row = 0; row < currentGeneration.numRows(); row++){
            for(int col = 0; col < currentGeneration.numColumns(); col++){
                nextGeneration.set(row, col, this.getNextCell(row, col));
            }
        }
        // updates currentGeneration
        for(int row = 0; row < currentGeneration.numRows(); row++){
            for(int col = 0; col < currentGeneration.numColumns(); col++){
                currentGeneration.set(row, col, nextGeneration.get(row, col));
            }
        }
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState cellState = currentGeneration.get(row, col);
        if(cellState == CellState.ALIVE){
            return CellState.DYING;
        }
        else if (cellState == CellState.DYING){
            return CellState.DEAD;
        } else {
            if(countNeighbors(row, col, CellState.ALIVE) == 2) {
                return CellState.ALIVE;
            }
            return CellState.DEAD;
        }
    }

    private int countNeighbors(int row, int col, CellState state) {
        int count = 0;
        if(currentGeneration.get(row, col).equals(state)){
            count--;
        }
        for(int dRow = -1; dRow < 2; dRow++) {
            if (row + dRow < currentGeneration.numRows() && row + dRow >= 0) {
                for (int dCol = -1; dCol < 2; dCol++) {
                    if (col + dCol < currentGeneration.numColumns() && col + dCol >= 0) {
                        if (currentGeneration.get(row + dRow, col + dCol).equals(state)) {
                            count++;
                        }
                    }
                }
            }
        }
        return count;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
